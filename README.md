# Drupal 8 Behat Demo

## Usage

1. `composer install`
2. `docker-compose up -d --force-recreate`

**TODO:** `--force-recreate` is an existing issue with [`selenium/standalone`](https://github.com/SeleniumHQ/docker-selenium/issues/91). Only happens when the container already exists.
